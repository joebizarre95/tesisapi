from django.shortcuts import render
from django.shortcuts import get_object_or_404 , get_list_or_404
from django.http import HttpResponse , JsonResponse
from django.contrib.auth import authenticate
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import urlencode 
from django.http import QueryDict
#rest framework
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import mixins 
from rest_framework import generics 

#serializers
from .serializers import UserSerializer , PublicacionSerializer

#models 
from .models import profile , publicacion

#functions 
from .functions import (object_login , get_single , update_single , get_all_publications, object_save_post)


from rest_framework import viewsets
from django.core import serializers
import json


from django.db.models import Q 
from django.contrib.auth.models import User



class getUserData(APIView):
	def post(self, request):
		#data = request.POST.get('username')
		response = get_user(data)
		return Response(response)


class getUserDetail(APIView):
	def get(self, request, pk):
		response = get_single(pk, UserSerializer, profile)
		return Response(response)


class UpdateData(APIView):
	def get(self, request, pk):
		response = get_single(pk, UserSerializer, profile)
		return Response(response)


class UpdateDataRequest(APIView):		
	def put(self, request, pk):
		response = update_single(pk, profile, UserSerializer, request)
		return HttpResponse()


class GetPublicaciones(APIView):
	def get(self, request, pk):
		response = get_all_publications(pk, PublicacionSerializer, publicacion)
		return Response(response.data)
	"""def get(self, request, pk):
		response = get_all_publications(pk, PublicacionSerializer, publicacion)
		return Response(response)"""




#.values('id','user','titulo','fecha_evento','ubicacion','descripcion','foto')
def getMyPublish(request, pk):
	lista = serializers.serialize('json', publicacion.objects.filter(user_id=pk))
	return HttpResponse(lista)



class GetImageUrl(APIView):
	def get(self, request, imagepath):
		response = get_image(imagepath)
		return Reponse(response)


class NewPublicacion(APIView):
	def post(self, request):
		#response = print(request.data)
		response = object_save_post(PublicacionSerializer, request)
		return Response(200)
		


class edAddUser(APIView):
	def post(self,request):
		response = object_save_post(UserSerializer , request)
		return HttpResponse(200)

	"""def post(self, request):
		print(request)
	serializer_class = PublicacionSerializer
	queryset = profile.objects.all()
	models = profile

	def post(request):
		return self.create(request)"""

@csrf_exempt
def search(request):
	data = urlencode(json.loads(request.body))
	user = request.POST = QueryDict(data , mutable=True)
	if "user" in user:
		print(user["user"])
		objeto = user["user"]
	
	if request:
		results =  User.objects.filter(username=objeto).distinct()
		result_serialize = serializers.serialize('json',results)
		#result_serialize = UserSerializer(results)
		#results = User.objects.all()
		if (len(results) != 0):
			return HttpResponse(result_serialize)
		else:
			results = User.objects.filter(email=objeto).distinct()
			result_serialize = serializers.serialize('json',results)
			#result_serialize = UserSerializer(results)
			#results = User.objects.all()
			if (len(results) != 0):
				return HttpResponse(result_serialize)
			else:
				return HttpResponse('El usuario no ha sido encontrado')
	else:
		results = 'no se encuentra el usuario'
		return HttpResponse(results)


