# Generated by Django 2.0.3 on 2018-05-01 03:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20180428_2318'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='profile',
            options={'ordering': ('id',)},
        ),
    ]
