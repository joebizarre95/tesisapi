from rest_framework.response import Response
from rest_framework import status 
from django.shortcuts import get_object_or_404 , get_list_or_404
from django.contrib.auth import authenticate
from django.http import HttpResponse
import json
from django.contrib.auth.models import User 
from .models import profile , publicacion
from django.core import serializers

def object_login(serializer, request):
	username =  request.POST.get('username')
	password =  request.POST.get('password')
	user = authenticate(request, username=username, password=password)
	if user is not None:
		return Response('1')
	return Response('0')



#get single
def get_single(pk, serializer, model):
	obj = get_object_or_404(model, user_id=pk) 
	res = serializer(obj)
	return res.data



def update_single(pk, model, serializer, request):
	obj = get_object_or_404(model, user_id=pk)
	res = serializer(obj, data=request.data)
	if res.is_valid():
		res.save()
		return Response(res.data, status=200)
	return Response(res.errors, status=status.HTTP_400_BAD_REQUEST)



def get_all_publications(pk, serializer, model):
	obj = model.objects.filter(user_id = pk)
	res = serializer(obj ,many=True)
	return res



def get_especific_publication(pk, serializer, model):
	obj = publicacion.objects.get(model , pk=pk)
	res = serializer(obj, many=True)
	return res.data





def get_image(imagepath):
	url = 'http://127.0.0.1:8000/media'+imagepath
	return url


def object_save_post( serializer,  request):
	obj = serializer(data=request.data)
	if obj.is_valid():
		obj.save()
		return Response(serializer.data)
	return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


