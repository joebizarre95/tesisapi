from django.contrib import admin
from .models import profile , publicacion , categorias , followers

# Register your models here.

@admin.register(profile)
class Adminprofile(admin.ModelAdmin):
	lit_display = ('id','user','foto_perfil','ubicacion','fecha_nacimiento','nombre','apellido','bio')


@admin.register(publicacion)
class AdminPublicacion(admin.ModelAdmin):
	lit_display = ('id','user_id','titulo','fecha_evento','ubicacion','descripcion','foto')


@admin.register(categorias)
class AdminCategorias(admin.ModelAdmin):
	lit_display = ('pk', 'categoria', )


@admin.register(followers)
class AdminFollowers(admin.ModelAdmin):
	lit_display = ('pk', 'user' , 'follower' ,)