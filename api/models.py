from django.db import models
from django.contrib.auth.models import User 
from django.db.models.signals import post_save
from django.dispatch import receiver 
from django.utils import timezone

# Create your models here.


class profile(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	foto_perfil = models.ImageField(blank=True, upload_to='')
	ubicacion = models.CharField(max_length=255, blank=True)
	fecha_nacimiento = models.DateField(blank=True, null=True)
	nombre = models.CharField(max_length=255, blank=True)
	apellido = models.CharField(max_length=255, blank=True)
	bio = models.CharField(max_length=255, blank=True)

	@property
	def full_image(self):
		url = 'http://127.0.0.1:8000'
		return self.url + self.foto_perfil

	def __str__(self):
		return '{}'.format(
			self.user,
			self.foto_perfil,
			self.ubicacion,
			self.fecha_nacimiento,
			self.nombre,
			self.apellido,
			self.bio,
			)

	class Meta:
		ordering = ('id',)




class categorias(models.Model):
	categoria = models.CharField(max_length = 255 , blank=True)

	def __str__(self):
		return '{}'.format(
			self.categoria)

	class Meta:
		ordering = ('id',)



class publicacion(models.Model):
	user = models.ForeignKey(User, on_delete=False)
	categoria = models.ForeignKey(categorias, on_delete=False, blank=True, null= True)
	titulo = models.CharField(max_length=55)
	fecha_evento = models.DateField(blank=False)
	ubicacion = models.CharField(max_length=100,blank=True)
	descripcion = models.CharField(max_length=100)
	foto = models.ImageField(blank=True, upload_to='', null=True)

	def __str__(self):
		return '{}'.format(
			self.user,
			self.titulo,
			self.fecha_evento,
			self.ubicacion,
			self.descripcion,
			self.foto,
			)

	class Meta:
		ordering = ('id',)



class followers(models.Model):
	user = models.ForeignKey(User, on_delete=False)
	follower = models.ForeignKey(User, related_name="folloer", on_delete=False)

	def __str__(self):
		return '{}'.format(
			self.user,
			self.follower
			)

	class Meta:
		ordering = ('id',)