from django.contrib.auth import get_user_model
from rest_framework import serializers 
from .models import profile , publicacion
from django.contrib.sites.models import Site
# Get the UserModel
UserModel = get_user_model()





class UserDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = ('pk', 'username', 'email', 'first_name', 'last_name')
        read_only_fields = ('email', )



class UserSerializer(serializers.ModelSerializer):
	image_url = serializers.SerializerMethodField()
	#datauser = UserDetailsSerializer()

	class Meta:
		model = profile 
		fields = ('user','foto_perfil', 'ubicacion', 'fecha_nacimiento', 'nombre','apellido','bio' , 'image_url' )

	def get_image_url(self, profile):
		return Site.objects.get_current().domain + profile.foto_perfil.url 




class PublicacionSerializer(serializers.ModelSerializer):
	image_url = serializers.SerializerMethodField()
	#datauser = UserSerializer()


	class Meta:
		model = publicacion
		fields = ('pk','user','categoria','titulo','fecha_evento','ubicacion','descripcion','foto' , 'image_url', )

	def get_image_url(self, publicacion):
		return Site.objects.get_current().domain + publicacion.foto.url 