"""apijoin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings 
from django.conf.urls.static import static 
from django.views.static import serve

from django.contrib import admin
from django.urls import path , include
from .views import (getUserData  , getUserDetail , UpdateData , UpdateDataRequest, GetPublicaciones , GetImageUrl , NewPublicacion ,
                      edAddUser, )
from .views import getMyPublish , search
urlpatterns = [
   #login viejo path('LoginAPI', LoginAPI.as_view(), name="LoginAPI"),
   #login over poguel
   path('LoginAPI/', include('rest_auth.urls')),
   path('LoginAPI/registration', include('rest_auth.registration.urls')),

   #despues del login
   path('getUserDetail/<int:pk>/', getUserDetail.as_view()),
   path('UpdateData/<int:pk>', UpdateData.as_view()),
   path('UpdateDataRequest/<int:pk>' ,UpdateDataRequest.as_view()),
   path('GetPublicaciones/<int:pk>', GetPublicaciones.as_view()),
   path('GetImageUrl/<int:pk>', GetImageUrl.as_view()),
   path('NewPublicacion', NewPublicacion.as_view()),

   path('getMyPublish/<int:pk>', getMyPublish, name="getMyPublish"),

   path('edAddUser/', edAddUser.as_view()),

   path('search/', search, name="search"),
]

